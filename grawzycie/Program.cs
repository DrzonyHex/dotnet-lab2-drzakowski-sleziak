﻿// Gra w zycie
// Tworcy: Sleziak Rafal, Drzakowski Mateusz


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace ConsoleApp3
{

    class Program
    {
        class Cell
        {
            public int Value { get; set; }
            public int Neighbours { get; set; }

            public Cell()
            {
                Neighbours = 0;
                Value = 0;
            }
        }

        class Board
        {
            public int iteration;
            int nr_cells;
            public int dead;
            static int size_board;
            public Cell[,] table;

            static int set_value()
            {
                string line = Console.ReadLine();
                int a = int.Parse(line);
                while (true)
                {
                    if (a != 0)
                        return a;
                    else
                    {
                        Console.WriteLine("Podaj liczbe rozna od 0! ");
                        line = Console.ReadLine();
                        a = int.Parse(line);
                    }
                }
            }

            public Board()
            {
                Console.WriteLine("Podaj rozmiar tablicy: ");
                size_board = set_value();
                table = new Cell[size_board, size_board];
                Console.WriteLine("Podaj startowa liczbe komorek: ");
                nr_cells = set_value();
                dead = 0;
                iteration = 0;
                Initialize();
            }

            void Initialize() //inicjalizuje plansze i wypelnia ja zywymi komorkami
            {
                int iter = 0;
                int j, z;

                for (int i = 0; i < size_board; i++)
                {
                    for (int ii = 0; ii < size_board; ii++)
                    {
                        table[i, ii] = new Cell();
                    }
                }

                while (iter < nr_cells)
                {
                    Random rand2 = new Random();
                    j = rand2.Next(0, (size_board - 1));
                    Random rand3 = new Random();
                    z = rand3.Next(0, (size_board - 1));

                    if (table[j, z].Value == 0)
                    {
                        table[j, z].Value = 1;
                        iter++;
                    }
                }
            }

            public void Cout() //wyswietla plansze
            {
                for (int c = 0; c < size_board; c++)
                {
                    for (int u = 0; u < size_board; u++)
                    {
                        Console.Write(table[c, u].Value);
                    }
                    Console.WriteLine();
                }
            }

            public void Check_neighbours()
            {
                for (int x = 0; x < size_board; x++)
                {
                    for (int y = 0; y < size_board; y++)
                    {                       
                        for (int i=x-1; i <= x+1; i++)
                        {
                            for(int j=y-1; j<=y+1; j++)
                            {
                                if(i>= 0 && j>=0 && i<size_board && j<size_board)
                                {
                                    if (table[i, j].Value == 1)
                                    {
                                        if (x != i || y != j)
                                            table[x, y].Neighbours++;
                                    }                                       
                                }                                   
                            }
                        }
                    }
                }
            }//checkneighbours

            public void Make_game()
            {
                dead = 0;
                for (int x = 0; x < size_board; x++)
                {
                    for (int y = 0; y < size_board; y++)
                    {
                        //"kazda zywa komorka": 

                        if (table[x, y].Value == 1 && (table[x, y].Neighbours < 2 || table[x, y].Neighbours > 3))
                            table[x, y].Value = 0;
                        //"kazda martwa komorka":
                        if (table[x, y].Value == 0 && table[x, y].Neighbours == 3)
                            table[x, y].Value = 1;
                        if (table[x, y].Value == 0)
                        {
                            dead += 1;
                        }
                        table[x, y].Neighbours = 0;//czyszczenie "sasiadow" do zera, aby zliczyc ich ponownie przy kolejnym dniu
                    }
                }
                iteration++; //kolejny dzien
            }

            public bool Check_life() // sprawdza, czy na planszy jest jeszcze jakas zywa komorka
            {
                if (dead == size_board * size_board)
                    return false;
                else
                    return true;
            }

            void CoutN() //wyswietla plansze z liczba sasiadow
            {
                for (int c = 0; c < size_board; c++)
                {
                    for (int u = 0; u < size_board; u++)
                    {
                        Console.Write(table[c, u].Neighbours);
                    }
                    Console.WriteLine();
                }
            }
        }

        static void Main(string[] args)
        {
            int days_of_life = 0;
            Board First_match = new Board();

            Console.WriteLine("Hello in Gameoflife:");
            Console.Write("Dzien: "); Console.Write(days_of_life); Console.WriteLine();

            First_match.Cout();
            Console.WriteLine("Naciśnij dowolny klawisz, aby nastąpil kolejny 'dzien':");
            Console.ReadKey();

            while (First_match.Check_life() == true)
            {
                days_of_life++;
                Console.Write("Dzien: "); Console.Write(days_of_life); Console.WriteLine();
                First_match.Check_neighbours(); //sprawdzam liczbe sasiadow
                First_match.Make_game(); //wymieram/ozywiam komorki
                First_match.Cout(); //wyswietlam plansze
                Console.WriteLine("Naciśnij dowolny klawisz, aby nastąpil kolejny 'dzien':");
                Console.ReadKey();


            }
            Console.Write("Koniec gry.\nLiczba przeżytych 'dni': ");
            Console.Write(days_of_life - 1);
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}